package restaurant.mvpservices.config;


import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import restaurant.mvpservices.security.JwtAuthenticationFilter;
import restaurant.mvpservices.security.JwtAuthorizationFilter;

@RequiredArgsConstructor //wieloargumentowy konstruktor dla finalnych zmiennych
@EnableWebSecurity  //włączenie security, rejestrowanie klasy wraz z włączeniem security
@EnableGlobalMethodSecurity(prePostEnabled = true) //włącza działanie adnotacji która sprawdza dostępy do metod
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private final UserDetailsService userDetailsService;
    private final PasswordEncoder passwordEncoder;
    private final ObjectMapper objectMapper;



    @Override //ctrl + o
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable(); // wyłączenie tokena csrf dla wszystkich endpointów w aplikacji

        http.cors()
                .and()
                .addFilter(new JwtAuthenticationFilter(authenticationManager(), objectMapper))//objectMapper jako defaultowy bean w springu
                .addFilter(new JwtAuthorizationFilter(authenticationManager())) //zarejestrowanie authorizationfilter
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);//aplikacje z innych serwerow nie mogą się komunikować z naszym serwerem, ręcznie trzeba włączyć z jakich serwerów komunikacja możliwa
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder); //wzorzec projektowy builder

    }
}
