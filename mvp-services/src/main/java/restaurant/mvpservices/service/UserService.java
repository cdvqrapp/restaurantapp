package restaurant.mvpservices.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import restaurant.mvpservices.model.dao.User;

public interface UserService {

    User save(User user); //metoda abstrakcyjna nie posiada implementacji

    User update(User user, Long id);

    User findById(Long id);

    void deleteById(Long id);

    User findCurrentUser();

    Page<User> getPage(Pageable pageable);

}
