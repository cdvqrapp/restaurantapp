package restaurant.mvpservices.service;

import restaurant.mvpservices.model.dao.Product;

import java.util.List;

public interface BasketService {
    void deleteProductFromBasket(Long productId);

    void updateBasket(Long productId, Integer quantity);

    void clearBasket();

    List<Product> getProducts();
}
