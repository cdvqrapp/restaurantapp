package restaurant.mvpservices.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import restaurant.mvpservices.exception.QuantityExceededException;
import restaurant.mvpservices.model.dao.Basket;
import restaurant.mvpservices.model.dao.Product;
import restaurant.mvpservices.model.dao.User;
import restaurant.mvpservices.repository.BasketRepository;
import restaurant.mvpservices.service.BasketService;
import restaurant.mvpservices.service.ProductService;
import restaurant.mvpservices.service.UserService;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class BasketServiceImpl implements BasketService {
    private final BasketRepository basketRepository;
    private final ProductService productService;
    private final UserService userService;



    @Override
    public void deleteProductFromBasket(Long productId) {
        User userDb = userService.findCurrentUser();
        basketRepository.deleteByUserIdAndProductId(userDb.getId(), productId);

    }

    @Override
    public void updateBasket(Long productId, Integer quantity) {

        User userDb = userService.findCurrentUser();
        basketRepository.findByUserIdAndProductId(userDb.getId(), productId).ifPresentOrElse(basket -> {
            if (basket.getProduct().getQuantity() < basket.getQuantity() + quantity) {
                throw new QuantityExceededException("Wrong quantity for product " + basket.getProduct().getName() + " During updating basket ");
            }
            basket.setQuantity(basket.getQuantity() + quantity);

            basketRepository.save(basket);


        }, () -> {
            Product product = productService.findById(productId);
            if (quantity > product.getQuantity()) {
                throw new QuantityExceededException("Wrong quantity for product" + product.getName() + " During Added product to basket");

            }
            basketRepository.save(new Basket(null, userDb, product, quantity));
        });
    }
    @Override
    public void clearBasket() {
        User userDb = userService.findCurrentUser();
        basketRepository.deleteByUserId(userDb.getId());

    }

    @Override
    public List<Product> getProducts() {
        User userDb = userService.findCurrentUser();


        return basketRepository.findByUserId(userDb.getId()).stream()
                .map(basket -> basket.getProduct()) //jak zrobic zeby napisac wiecej niz jedna linijke w lambdzie
                .collect(Collectors.toList());
    }
}


