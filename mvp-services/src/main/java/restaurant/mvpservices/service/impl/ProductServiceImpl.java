package restaurant.mvpservices.service.impl;

import lombok.RequiredArgsConstructor;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import restaurant.mvpservices.model.dao.Product;
import restaurant.mvpservices.repository.ProductRepository;

import restaurant.mvpservices.service.ProductService;

import javax.transaction.Transactional;


@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;


    @Override
    public Product save(Product product) {
        return productRepository.save(product);
    }


    @Override
    @Transactional
    //sprawdza czy stara wartosc z db oraz nowa wartosc sa od siebie rozne -> jezeli prawda to wykonuje update
    public Product update(Product product, Long id) {
        Product productDb = findById(id);
        productDb.setName(productDb.getName());
        productDb.setQuantity(productDb.getQuantity());
        productDb.setPrice(productDb.getPrice());
        productDb.setCode(productDb.getDescription());
        product.setCode(productDb.getCode());

        return productDb;
    }

    @Override
    public Product findById(Long id) {
        return productRepository.getById(id);
    }

    @Override
    public void deleteById(Long id) {
        productRepository.deleteById(id);
    }

    @Override
    public Page<Product> getPage(Pageable pageable) {
        return productRepository.findAll(pageable);
    }
}
