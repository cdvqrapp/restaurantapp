package restaurant.mvpservices.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import restaurant.mvpservices.model.dao.Product;
import restaurant.mvpservices.model.dao.User;
import restaurant.mvpservices.repository.ProductRepository;
import restaurant.mvpservices.repository.RoleRepository;
import restaurant.mvpservices.repository.UserRepository;
import restaurant.mvpservices.security.SecurityUtils;
import restaurant.mvpservices.service.UserService;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.Collections;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;


    @Override
    public User save(User user) {

        roleRepository.findByName("ROLE_USER").ifPresent(role -> user.setRoles(Collections.singletonList(role))); //zwracanie jednoelementowej listy (singleton)
        //interfejsy funkcuyjne powtórzyć supplier,consumer itd..
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepository.save(user);
    }


    @Override
    @Transactional
    //sprawdza czy stara wartosc z db oraz nowa wartosc sa od siebie rozne -> jezeli prawda to wykonuje update
    public User update(User user, Long id) {
        User userDb = findById(id);
        userDb.setEmail(user.getEmail());
        userDb.setFirstName(user.getFirstName());
        userDb.setLastName(user.getLastName());

        return userDb;
    }

    @Override
    public User findById(Long id) {
        return userRepository.getById(id);
    }

    @Override
    public void deleteById(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    public Page<User> getPage(Pageable pageable) {
        return userRepository.findAll(pageable);
    }

    @Override
    public User findCurrentUser() {

        return userRepository.findByEmail(SecurityUtils.getCurrentUserEmail())
                .orElseThrow(() -> new EntityNotFoundException("User not logged in"));  //supplier
    }
}
