package restaurant.mvpservices.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import restaurant.mvpservices.model.dao.Product;

public interface ProductService {

    Product save(Product product);

    Product update(Product product, Long id);

    Product findById(Long id);

    void deleteById(Long id);

    Page<Product> getPage(Pageable pageable);
}
