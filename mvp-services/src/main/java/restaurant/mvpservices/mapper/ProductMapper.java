package restaurant.mvpservices.mapper;

import org.mapstruct.Mapper;
import restaurant.mvpservices.model.dao.Product;
import restaurant.mvpservices.model.dto.ProductDto;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ProductMapper {

    ProductDto productToProductDto(Product product);

    Product productDtoToProduct(ProductDto productDto);
    List<ProductDto> productsToProductsDto(List<Product> products);
}
