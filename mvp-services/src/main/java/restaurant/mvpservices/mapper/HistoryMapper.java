package restaurant.mvpservices.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.data.history.Revision;
import restaurant.mvpservices.model.dao.Product;
import restaurant.mvpservices.model.dao.User;
import restaurant.mvpservices.model.dto.ProductDto;
import restaurant.mvpservices.model.dto.UserDto;

@Mapper(componentModel = "spring")      //maven clean -> compile
public interface HistoryMapper {

    @Mapping(source = "entity.id", target = "id")
    @Mapping(source = "entity.firstName", target = "firstName")
    @Mapping(source = "entity.lastName", target = "lastName")
    @Mapping(source = "entity.email", target = "email")
    @Mapping(source = "requiredRevisionNumber", target = "revisionNumber")
    UserDto historyToUserDto(Revision<Integer, User> revision);  //revision - reprezentuje dane z tabelki audytowej


    @Mapping(source = "entity.id", target ="id")
    @Mapping(source = "entity.name", target ="name")
    @Mapping(source = "entity.quantity", target ="quantity")
    @Mapping(source = "entity.description", target ="description")
    @Mapping(source = "entity.price", target ="price")
    @Mapping(source = "entity.code", target ="code")
    @Mapping(source = "requiredRevisionNumber", target = "revisionNumber")
    ProductDto historyToProductDto(Revision<Integer, Product> revision);





}
