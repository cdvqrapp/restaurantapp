package restaurant.mvpservices.mapper;

import org.mapstruct.Mapper;

import org.mapstruct.Mapping;
import restaurant.mvpservices.model.dao.User;
import restaurant.mvpservices.model.dto.UserDto;

@Mapper(componentModel = "spring")
public interface UserMapper {

    @Mapping(target = "password", ignore = true)
    UserDto userToUserDto(User user);

    User userDtoToUser(UserDto userDto);
}
