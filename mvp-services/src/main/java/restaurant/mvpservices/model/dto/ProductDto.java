package restaurant.mvpservices.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@AllArgsConstructor
public class ProductDto {
    private Long id;
    @NotBlank
    private String name;
    private Integer quantity;
    @NotBlank
    private String description;
    @Min(value = 0, message = "The price shound not be less than 0")
    private Long price;
    @NotBlank
    private String code;
    private Integer revisionNumber;
}
