package restaurant.mvpservices.model.dto;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@AllArgsConstructor


public class UserDto {

    private Long id;

    private String firstName;
    private String lastName;
    @Email
    private String email;
    private String password;
    private String confirmPassword;
    private Integer revisionNumber;

}
