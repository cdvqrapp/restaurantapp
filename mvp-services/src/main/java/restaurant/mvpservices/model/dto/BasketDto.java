package restaurant.mvpservices.model.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Positive;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BasketDto {
    private Long productId;
    @Positive(message = "The quantity should not be less than 0")
    private Integer quantity;
}
