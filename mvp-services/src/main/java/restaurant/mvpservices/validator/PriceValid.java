package restaurant.mvpservices.validator;

import restaurant.mvpservices.validator.impl.PriceValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = PriceValidator.class)
public @interface PriceValid {


    String message() default "The price sholud be greater than 0! ";

    Class<?>[] groups() default {};  //włączanie i wyłączanie validatora w odpowiednich miejscach

    Class<? extends Payload>[] payload() default {};   //metoda defaultowa ? - cokolwiek co rozszerza klase payload

}