package restaurant.mvpservices.validator.impl;

import restaurant.mvpservices.model.dto.ProductDto;
import restaurant.mvpservices.validator.PriceValid;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PriceValidator implements ConstraintValidator<PriceValid, ProductDto> {
    @Override
    public boolean isValid(ProductDto productDto, ConstraintValidatorContext constraintValidatorContext) {
        return productDto.getPrice() > 0;
    }
}