package restaurant.mvpservices.validator;

import restaurant.mvpservices.validator.impl.PasswordValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented // - oznaczenie jako adnotacja
@Target(ElementType.TYPE)  //w którym miejscu będziemy mogli używać tej adnotacji (TYPE czyli dla klasy) FIELD dla pola METHOD dla metody, PARAMETER
@Retention(RetentionPolicy.RUNTIME) //validator działa w trakcie działania programu
@Constraint(validatedBy = PasswordValidator.class)  //podawanie klasy
public @interface PasswordValid {
    String message() default "Confirmed password should be the same ";

    Class<?>[] groups() default {};  //włączanie i wyłączanie validatora w odpowiednich miejscach

    Class<? extends Payload>[] payload() default {};   //metoda defaultowa ? - cokolwiek co rozszerza klase payload

}
