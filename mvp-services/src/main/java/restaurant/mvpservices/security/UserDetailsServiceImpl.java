package restaurant.mvpservices.security;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import restaurant.mvpservices.repository.UserRepository;

import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {
    //dostarcza użytkownika do logowania

    private final UserRepository userRepository;


    @Override
    @Transactional
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        return userRepository.findByEmail(email)
                .map(user -> new User(email, user.getPassword(), user.getRoles().stream() //interfejs funkcyjny function
                        .map(role -> new SimpleGrantedAuthority(role.getName())) //interfejs funkcyjny function
                        .collect(Collectors.toSet())))
                .orElseThrow(() -> new UsernameNotFoundException(email));  // interfejs funkcyjny supplier



        //dzieki temu mozna wywolac metody ktore nie sa statyczne
    }


}