package restaurant.mvpservices.security;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import restaurant.mvpservices.service.UserService;

@Service
@RequiredArgsConstructor
public class SecurityService {

    private final UserService userService;

    //sprawdzic czy id aktualnego usera zalogowanego jest rowne id usera z parametru funkcji

    public boolean hasAccessToUser(Long id) {

        return userService.findCurrentUser().getId().equals(id);
    }

}
