package restaurant.mvpservices.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import restaurant.mvpservices.mapper.UserMapper;
import restaurant.mvpservices.model.dao.User;
import restaurant.mvpservices.model.dto.UserDto;
import restaurant.mvpservices.service.UserService;
import restaurant.mvpservices.validator.group.Create;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/users")
@RequiredArgsConstructor

public class UserController {

    private final UserMapper userMapper;
    private final UserService userService;

    @Validated(Create.class) // włączenie naszej walidacji tylko dla tej metody ( dla grupy create)
    @PostMapping
    @PreAuthorize("hasRole('ADMIN') || isAnonymous()")  //admin i niezalogowany user
    public UserDto saveUser(@RequestBody @Valid UserDto user) {
        return userMapper.userToUserDto(userService.save(userMapper.userDtoToUser(user))); //userService odpowiada za zapis (usermapper mapuje użytkownika dto na użytkownika)
    }


    //@PathVariable - id podawane po /2  -wart id 2
    //@RequestParam - po ?parameter=wartosc&drugiparametr=wartosc2


    @GetMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public UserDto getUser(@PathVariable Long id)  /* cała linijka jest nagłówkiem funkcji */ {
        User user = userService.findById(id);
        return userMapper.userToUserDto(user);

    }

    @PutMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN') || @securityService.hasAccessToUser(#id) ") //spell  # odwołanie do parametru do ktorego zostala dodana adnotacja z parametru funkcji update user
    public UserDto updateUser(@RequestBody UserDto user, @PathVariable Long id) {
        return userMapper.userToUserDto(userService.update(userMapper.userDtoToUser(user), id));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public void deleteUser(@PathVariable Long id) {
        userService.deleteById(id);
    }

    @GetMapping
    @PreAuthorize("hasRole('ADMIN')") //tylko admin ma dostęp do metody
    public Page<UserDto> getUserPage(@RequestParam int page, @RequestParam int size) {          //sparametryzowany Page o UserDto
        return userService.getPage(PageRequest.of(page, size)).map(userMapper::userToUserDto); //wywołanie funkcji map na getPage i metodą ref zmapowanie na userDto
    }
}
