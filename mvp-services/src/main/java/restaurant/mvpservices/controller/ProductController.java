package restaurant.mvpservices.controller;

import lombok.RequiredArgsConstructor;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import restaurant.mvpservices.mapper.ProductMapper;
import restaurant.mvpservices.model.dto.ProductDto;
import restaurant.mvpservices.service.ProductService;
import restaurant.mvpservices.validator.group.Create;

import javax.validation.Valid;


@RestController
@RequestMapping("/api/products")
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;
    private final ProductMapper productMapper;


    @PreAuthorize("hasRole('ADMIN')")
    @Validated(Create.class)
    @PostMapping
    public ProductDto saveProduct(@RequestBody @Valid ProductDto product) {
        return productMapper.productToProductDto(productService.save(productMapper.productDtoToProduct(product)));
    }

    @GetMapping("/{id}")
    public ProductDto getProduct(@PathVariable Long id) {
        return productMapper.productToProductDto(productService.findById(id));
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ProductDto updateProduct(@RequestBody ProductDto product, @PathVariable Long id) {
        return productMapper.productToProductDto(productService.update(productMapper.productDtoToProduct(product), id));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public void deleteProduct(@PathVariable Long id) {
        productService.deleteById(id);
    }

    @GetMapping
    public Page<ProductDto> getProductPage(@RequestParam int page, @RequestParam int size) {
        return productService.getPage(PageRequest.of(page, size)).map(productMapper::productToProductDto);
    }
}
