package restaurant.mvpservices.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import restaurant.mvpservices.mapper.ProductMapper;
import restaurant.mvpservices.model.dto.BasketDto;
import restaurant.mvpservices.model.dto.ProductDto;
import restaurant.mvpservices.service.BasketService;

import javax.validation.Valid;
import java.util.List;

@Validated
@RestController
@RequestMapping("/api/baskets")
@RequiredArgsConstructor
public class BasketController {

    private final BasketService basketService;
    private final ProductMapper productMapper;

    @PutMapping
    @PreAuthorize("isAuthenticated()")
    public void updateBasket(@RequestBody @Valid BasketDto basketDto) {
        basketService.updateBasket(basketDto.getProductId(), basketDto.getQuantity());
    }
    @GetMapping
    @PreAuthorize("isAuthenticated()")
    public List<ProductDto> getProductsFromUserBasket() {
        return productMapper.productsToProductsDto(basketService.getProducts());
    }

    @DeleteMapping("/{productId}")
    @PreAuthorize("isAuthenticated()")
    public void deleteProductFromUserBasket(@PathVariable Long productId){  //nazwa sie musi zgadzac w mappingu
        basketService.deleteProductFromBasket(productId);
    }

    @DeleteMapping
    @PreAuthorize("isAuthenticated()")
    public void deleteProductsFromUserBasket(){
        basketService.clearBasket();
    }
}
