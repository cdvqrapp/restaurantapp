package restaurant.mvpservices.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import restaurant.mvpservices.mapper.HistoryMapper;
import restaurant.mvpservices.model.dto.ProductDto;
import restaurant.mvpservices.model.dto.UserDto;
import restaurant.mvpservices.repository.ProductRepository;
import restaurant.mvpservices.repository.UserRepository;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/history")
@PreAuthorize("hasRole('ADMIN')")
public class HistoryController {
    //wstrzykiwanie user repo
    // łatwiej pod względem testów jednostkowych

    private final UserRepository userRepository;
    private final ProductRepository productRepository;
    private final HistoryMapper historyMapper;



    @GetMapping("/user/{id}")
    Page<UserDto> getUserHistory(@PathVariable Long id, @RequestParam int page, @RequestParam int size) {       //revision tabelki audytowe
        return userRepository.findRevisions(id, PageRequest.of(page, size)).map(historyMapper::historyToUserDto);
    }

    @GetMapping("/product/{id}")
    Page<ProductDto> getProductHistory(@PathVariable Long id, @RequestParam int page, @RequestParam int size) {
        return productRepository.findRevisions(id, PageRequest.of(page, size)).map(historyMapper::historyToProductDto);
    }


}
