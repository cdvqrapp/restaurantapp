package restaurant.mvpservices.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import restaurant.mvpservices.model.dto.FieldErrorDto;

import javax.persistence.EntityNotFoundException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
//poziomy logów - debug, trace (bardziej szczegółowy debug), info, warning, error
@RestControllerAdvice
public class AdviceController {


    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(EntityNotFoundException.class)
        //nazwa klasy który zostanie przechwycony (exception)
    void handleEntityNotFoundException(EntityNotFoundException e) {

        log.error("Nieprawidłowy zakres", e);


    }

    @ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler(SQLIntegrityConstraintViolationException.class)
    void handleSQLIntegrityConstraintViolationException(SQLIntegrityConstraintViolationException e) {

        log.error("Zasób już istnieje: ", e);
    }


    @ResponseStatus(HttpStatus.BAD_REQUEST) // to jest konieczne bo inaczej spring zwróci 200 i będzie OK dla niego
    @ExceptionHandler(MethodArgumentNotValidException.class)
    List<FieldErrorDto> handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {

        log.warn("Brakuje danych", e);


        //       FieldError objectError = (FieldError) errorList.get(0); //nawiasy - kastowanie
//        System.out.println(objectError.getDefaultMessage());
//        System.out.println(objectError.getField());

        return e.getBindingResult().getAllErrors().stream()
                .map(objectError -> {
                    FieldError fieldError = (FieldError) objectError;
//na zmiennej można wywoływać metody które nie są statyczne

                    return new FieldErrorDto(fieldError.getDefaultMessage(), fieldError.getField());
                })
                .collect(Collectors.toList());
    }

}